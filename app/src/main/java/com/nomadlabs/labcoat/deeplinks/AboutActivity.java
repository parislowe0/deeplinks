package com.nomadlabs.labcoat.deeplinks;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

public class AboutActivity extends AppCompatActivity {

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        View[] openRepo = new View[] { findViewById(R.id.title_source_code_link),
        findViewById(R.id.summary_source_code_link),
        findViewById(R.id.isext_source_code_link) };

        View.OnClickListener doOpenRepo = new View.OnClickListener() {
            @Override public void onClick(View v) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://gitlab.com/nomadlabs/deeplinks")));
            }
        };
        for(View v : openRepo) {
            v.setOnClickListener(doOpenRepo);
        }
    }
}
